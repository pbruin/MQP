# Mathematics for Quantum Physics

*Mathematics for Quantum Mechanics* gives you a compact introduction
and review of the basic mathematical tools commonly used in quantum
mechanics. Throughout the course, we keep quantum mechanics
applications in mind, but at the core, this is still a mathematics
course. For this reason, applying what you learned to examples and
exercises is **crucial**!

:::{admonition} Learning goals

After following this course, you will understand the following
concepts and be able to apply them:
- Analysis
  - Taylor series
  - Fourier series and Fourier transforms
  - Differential equations
- Linear algebra
  - Matrix operations
  - Eigenvalues and eigenvectors
  - Diagonalisation
  - Hilbert spaces
- Probability theory
  - Random variables
  - Probability distributions
  - Central limit theorem

Throughout, the focus is on examples and applications.
:::

:::{admonition} Exercises

Each lecture note comes with a set of exercises.  TODO: more
:::

With these notes, our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below [TODO].

Whether you are a student taking this course. or an instructor reusing
the materials, we welcome all contributions, so check out the [course
repository](https://gitlab.com/pbruin/MQP/).  Please [let us
know](mailto:P.J.Bruin@math.leidenuniv.nl) if you find mistakes!

TODO: refer to original Mathematics for Quantum Physics and its
authors
