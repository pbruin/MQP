---
title: Fourier series
---

# Fourier series

The *Fourier series* of a function expresses the function as a linear
combination of complex exponentials.  This is extremely useful in many
situations in mathematics, physics and engineering.

In terms of linear algebra, the theory of Fourier series provides an
orthonormal basis for the Hilbert space of square-integrable functions
on the interval $[0,1]$; see also Section 8 of these notes.

## Definition and basic properties

```{admonition} Definition
Let $f$ be a (real or complex) function on the interval $[0,1]$.
The *Fourier series* of $f$ is the infinite series

$$
\sum_{n=-\infty}^\infty c_n \exp(2\pi i n x)
$$

where the complex numbers $c_n$ are defined by

$$
c_n = \int_0^1 f(x)\exp(-2\pi i n x)dx.
$$

We call these $c_n$ the *Fourier coefficients* of $f$.
```

One reason why Fourier series are useful is that the basis functions

$$
e_n(x) = \exp(2\pi i n x)
$$

form an *orthonormal system*: in Problem 1, you will show that these
functions satisfy

$$
\int_0^1 e_m(x)\overline{e_n(x)}dx =
\begin{cases}
1& \text{if }m=n,\\
0& \text{if }m\ne n.
\end{cases}
$$

There are also other ways of writing this series, namely via *Euler's
formula*

$$
\exp(i x)=\cos x + i\sin x.
$$

One can show that if $f$ is continuous, then the Fourier series of $f$
converges to $f$ at every point.  For an example where $f$ is
discontinuous, see Problem 2.

## Examples

Consider the function $f(x)=x$.  To find its Fourier series, we
compute the integrals defining the coefficients $c_n$ for all integers
$n$.  We have

$$
c_0 = \int_0^1 x dx = \frac{1}{2}.
$$

For $n\ge0$ we compute $c_n$ using integration by parts:

$$
\begin{align*}
c_n &= \int_0^1 x\exp(-2\pi i n x)dx\\
&= \left.-\frac{1}{2\pi i n}x\exp(-2\pi i n x)\right|_{x=0}^1
+\frac{1}{2\pi i n}\int_0^1\exp(-2\pi i n x)dx\\
&= \left(-\frac{1}{2\pi i n}+0\right)+0\\
&= -\frac{1}{2\pi i n}.
\end{align*}
$$

We conclude that the Fourier series of $f$ is

$$
\frac{1}{2}-\sum_{n\ne0}\frac{1}{2\pi i n}\exp(2\pi i n x),
$$

where the sum ranges over all non-zero integers $n$.  Combining
the terms corresponding to $n$ and $-n$ and using the identity

$$
-\frac{1}{2\pi i n}\exp(2\pi i n x)
+ \frac{1}{2\pi i n}\exp(2\pi i (-n) x)
= -\frac{1}{\pi n}\sin(2\pi n x),
$$

we can simplify this to

$$
\frac{1}{2}-\sum_{n=1}^\infty\frac{1}{\pi n}\sin(2\pi n x),
$$

TODO: more examples

***

## Problems

1.  Prove the orthogonality relation

	$$
	\int_0^1 e_m(x)\overline{e_n(x)}dx =
	\begin{cases}
	1& \text{if }m=n,\\
	0& \text{if }m\ne n.
	\end{cases}
	$$

2.  Consider the function $f$ on the interval $[0,1]$ defined by

	$$
	f(x) = \begin{cases}
	1& \text{if }0\le x\le 1/2,\\
	0& \text{if }1/2<x\le 1.
	\end{cases}
	$$

	1. Compute the Fourier series of $f$.
	2. Compute the value of this Fourier series at $x=1/2$.
	
