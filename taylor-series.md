---
title: Taylor series
---

# Taylor series

*Taylor series* are a useful way of approximating a function by
simpler functions, namely polynomials.

## Definition and basic properties

```{admonition} Definition
Let $f$ be a (real or complex) function that is infinitely differentiable
at a point $a$.  The *Taylor series* of $f$ at $a$ is

$$
f(a) + f'(a) (x - a) + \frac{f''(a)}{2}(x - a)^2 + \cdots
= \sum_{n=0}^\infty \frac{f^{(n)}(a)}{n!}(x-a)^n.
$$
```

For almost all functions $f$ that one encounters in practice, the
Taylor series of $f$ will converge to $f$.  However, there are
exceptions; see the exercises.

## Important examples

### The exponential function

Consider the function $f(x)=\exp(x)$.  To compute its Taylor series at
$x=0$, note that all derivatives of $f$ are equal to $f$ itself, and
take the value $1$ at $x=0$.  This gives

$$
\exp(x) = 1 + x + \frac{1}{2}x^2 + \frac{1}{6}x^3+\cdots
= \sum_{n=0}^\infty\frac{1}{n!}x^n.
$$

### The logarithm

TODO: actually function $\log(1-x)$

### Trigonometric functions

TODO: sin, cos

***

## Problems

1.  Compute the Taylor series of the function

	$$
	f(x) = \frac{1}{1-x}.
	$$

2.  Consider the function

	$$
	f(x) = \begin{cases}
	\exp(-1/x^2)& \text{if }x\ne0,\\
	0& \text{if }x=0.
	\end{cases}
	$$

	Show that the Taylor series of $f$ at $0$ is identically zero.
